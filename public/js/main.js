
document.addEventListener("DOMContentLoaded", function() { 
    document.querySelectorAll('textarea, input').forEach(function(e) {
        if(e.value === '') e.value = localStorage.getItem(e.name, e.value);
        e.addEventListener('input', function() {
            localStorage.setItem(e.name, e.value);
        })
    })
});

$('form').submit(function(){
    $.post(
        'https://formcarry.com/s/vQzo1TymPj', // адрес обработчика
         $("form").serialize(), // отправляемые данные          
        
        function(msg) { // получен ответ сервера  
            $('form').hide('slow');
            
        }
    );
    alert("Данные успешно отправлены!");
    $('#name').val('');
    $('#e-mail').val('');
    document.getElementById('mes').value= '';
    return false;
});